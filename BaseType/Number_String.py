#!/usr/bin/env python3
# -*- coding: utf-8 -*-
print ("\n\n Number | String:")
#
'''
Number 5 种类型:
	整型int(十六进制0x/八进制0o),
	长整型(无限大的整数,后面跟着一个大写或小写的L),
	浮点型float,
	布尔型bool,
	复数complex(实部a和虚部b都是浮点型)


int(x) 将x转换为一个整数。
float(x) 将x转换到一个浮点数。
complex(x) 将x转换到一个复数，实数部分为 x，虚数部分为 0。
complex(x, y) 将 x 和 y 转换到一个复数，实数部分为 x，虚数部分为 y。x 和 y 是数字表达式。

'''


#加减乘除 + - * / % 
#/除,得浮点型	//除得整型     **乘方

'''
String使用引号('或")来创建字符串,没有C语言里面字符说法,只有字符串.
'''

a, b, c, d, e = 20, 5.5, True, 4+3j, 'hello world'
print(type(a), type(b), type(c), type(d), type(e))
del a,b,c,d,e
#print (a,b,c,d,e)		#报错,对象已被删除


str = 'Hello World!'
print (str)          # 输出字符串
#Python中的字符串有两种索引方式，从左往右以0开始，从右往左以-1开始。
print (str[0:-1])    # 输出第一个个到倒数第二个的所有字符
print (str[0])       # 输出字符串第一个字符
print (str[6:11])    # 输出从第7个开始到第11个的字符
print (str[6:])      # 输出从第7个开始的后的所有字符
print (str * 2)      # 输出字符串两次
print (str + "Python") 	# 连接字符串
print ('h' in str) 		# 判断'h'是否在str内
print ('h' not in str) 		# 判断'h'是否不在str内

if( "H" in str) :
    print("H 在变量 str 中")
else :
	print("H 不在变量 str 中")

if( "h" not in str) :
    print("h 不在变量 str 中")
else :
	print("h 在变量 str 中")
	


#与 C 字符串不同的是，Python str[0] = 'H'会导致错误,只能更新(整体修改)字符串
str = str[:6] + 'Python!'
print (str)

#使用反斜杠(\)转义特殊字符，如果你不想让反斜杠发生转义，可以在字符串前面添加一个 r或者R，表示原始字符串
print( ' hello \n world')
print(r' hello \n world')

#在 Python 中，字符串格式化使用与 C 中 sprintf 函数一样的语法。
print ("我叫 %s 今年 %d 岁!" % ('小明', 10))

#python三引号('''或""")允许一个字符串跨多行，字符串中可以包含换行符、制表符以及其他特殊字符。实例如下
para_str = '''这是一个多行字符串的实例
多行字符串可以使用制表符
TAB ( \t )。
也可以使用换行符 [ \n ]。
一般用于HTML或者SQL等
'''
print (para_str)
'''
http://www.runoob.com/python3/python3-string.html

capitalize()
将字符串的第一个字符转换为大写

center(width, fillchar)
返回一个指定的宽度 width 居中的字符串，fillchar 为填充的字符，默认为空格。	

count(str, beg= 0,end=len(string))
返回 str 在 string 里面出现的次数，如果 beg 或者 end 指定则返回指定范围内 str 出现的次数
'''














