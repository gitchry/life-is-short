#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Tuple and list are very smilar,but once the tuple cannot modify initialization
Because tuple is immutable, so the code with more secure.
So, tuple cannot use append(),insert(),pop()....
If possible, try to use tuple instead of list 
'''
classmates = ('Michael', 'Bob', 'Tracy')
print('classmates = %s' % str(classmates))
print('len(classmates) = %d' % len(classmates))
print('classmates[-1] = %s' % classmates[-1])

#When defining the tuple, you must initialize the its elements
t = (1, 2)

#When defining an empty tuple
t = ()

#When defining a only one element of the tuple, 
t = (1,)		#wrong: t = (1), doing so is equal to: t = 1

t = ('a', 'b', ['A', 'B'])
print("t =",t)
t[2][0] = 'X'
t[2][1] = 'Y'
print("t[2][0] = 'X'\nt[2][1] = 'Y'\nt =",t)

#why? -- change is the list, rather than the elements of the tuple, the elements of the tuple is not replaced by other list or data

tuple = ( 'abcd', 786 , 2.23, 'runoob', 70.2  )
tinytuple = (123, 'runoob')

print ('tuple               : ',tuple)             # 输出完整元组
print ('tuple[0]            : ',tuple[0])          # 输出元组的第一个元素
print ('tuple[1:3]          : ',tuple[1:3])        # 输出从第二个元素开始到第三个元素
print ('tuple[2:]           : ',tuple[2:])         # 输出从第三个元素开始的所有元素
print ('tinytuple * 2       : ',tinytuple * 2)     # 输出两次元组
print ('tuple + tinytuple   : ',tuple + tinytuple) # 连接元组






