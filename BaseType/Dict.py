#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
dict内部存放的顺序和key放入的顺序是没有关系的。

和list比较，dict有以下几个特点：
    1.查找和插入的速度极快，不会随着key的增加而变慢；
    2.需要占用大量的内存，内存浪费多。

而list相反：
    1.查找和插入的时间随着元素的增加而增加；
    2.占用空间小，浪费内存很少。

所以，dict是用空间来换取时间的一种方法。
"""

classmates = ['Michael', 'Bob', 'Tracy']
scores = [95, 75, 85]
d = {'Michael': 95, 'Bob': 75, 'Tracy': 85}

print("classmates =",classmates)
print("scores =",scores)
print("d =",d)
print("d['Michael'] =",d['Michael'])

d['Adam'] = 67
d['Jack'] = 90
d['Jack'] = 88
print("d =",d)


print("d['Thomas'] =",'Thomas' in d)
print("d.get('Thomas')=",d.get('Thomas'))
print("d.get('Thomas',-1)=",d.get('Thomas',-1))
#返回None的时候Python的交互式命令行不显示结果。

d.pop('Bob')
print("d =",d)

print()

dict = {}
#往dict中增加键/值
dict[0]         = "0 目录"
dict['1']       = "1 前言"
dict[2]         = "2 正文"
dict['three']   = "3 结束"
t = ('a', 'b',)   #此处t = ('a', 'b', ['A', 'B'])会出错:因为元素里面包含了不可变元素list
dict[t]   = "4 测试"

print (dict)       		
print (dict['three'])        # 输出键为 'three' 的值
print (dict[2])             # 输出键为 2 的值

del dict[0];      # 删除键是0的条目
dict.clear();     # 清空词典所有条目
del dict ;        # 删除词典
print(dict)
print()

selfdict = {'name': 'chry','age':23, 'sex': 'male'}
print ("selfdict String :%s" % str(selfdict))
print (selfdict.keys())     # 输出所有键
print (selfdict.values())   # 输出所有值

"""
dict可以用在需要高速查找的很多地方，在Python代码中几乎无处不在，正确使用dict非常重要，需要牢记的第一条就是dict的key必须是不可变对象。

这是因为dict根据key来计算value的存储位置，如果每次计算相同的key得出的结果不同，那dict内部就完全混乱了。这个通过key计算位置的算法称为哈希算法（Hash）。

要保证hash的正确性，作为key的对象就不能变。在Python中，字符串、整数等都是不可变的，因此，可以放心地作为key。而list是可变的，就不能作为key.
"""






