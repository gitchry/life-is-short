#!/usr/bin/env python3
# -*- coding: utf-8 -*-


classmates = ['Michael', 'Bob', 'Tracy']
print('classmates = %s' % str(classmates))
print('len(classmates) = %d' % len(classmates))
print('classmates[-1] = %s' % classmates[-1])

classmates.append('Adam')
print("classmates.append('Adam'), classmates = %s" % str(classmates))

classmates.insert(1, 'Jack')
print("classmates.insert(1, 'Jack'), classmates = %s" % str(classmates))

classmates.pop()	#meanwhile,return target element
print("classmates.pop(), classmates = %s" % str(classmates))

classmates[1] = 'Sarah'
print("classmates[1] = 'Sarah', classmates = %s" % str(classmates))

S = [True, 94]
L = ['python', 'java', S, 12.0]
print('len(L) = %d, L = %s' % (len(L),str(L)))
print('L[2][1] = %s' % (str(L[2][1])))

classmates = ('Michael', 'Bob', 'Tracy')

print()

list = [ 'abcd', 786 , 2.23, 'runoob', 70.2 ]
tinylist = [123, 'runoob']
print ('list            :',list)            
print ('tinylist        :',tinylist)           

print ('list[0]         :',list[0])         # 输出列表第一个元素
print ('list[1:-1]      :',list[1:-1])      # 从第二个开始输出到倒数第二个的所有字符
print ('list[2:]        :',list[2:])        # 输出从第三个元素开始的所有元素
print ('tinylist * 2    :',tinylist * 2)    # 输出两次列表
print ('list + tinylist :',list + tinylist) # 连接列表


list[0] = 9
list[2:4] = []   # 删除 list[2],list[3]
del list[2]
print('list[0] = 9\nlist[2:4] = []\ndel list[2]\nlist = :',list)    
print ('\n') 



