#!/usr/dev/env python3
# -*- coding: utf-8 -*-

# set()参数只能是一个字符串   可以进行集合运算 去重
a = set('abracadabra')
b = set('alacazam')

print("a = set('abracadabra'),  a =",a)
print("b = set('alacazam'),     b =",b)
print('a和b的差集               -- a - b =', a - b)     # a和b的差集
print('a和b的并集               -- a | b =', a | b)     # 
print('a和b的交集               -- a & b =', a & b)     # 
print('a和b中不同时存在的元素   -- a ^ b =', a ^ b)     # a和b中不同时存在的元素


immutable = ({'str', 12, 2+3j, ('Tuple',52.5)})
print('immutable =',immutable)   
#通过add(key)方法可以添加元素到set中，可以重复添加，但不会有效果：
immutable.add('Hello')
immutable.add(12)
print('immutable =',immutable)   
#通过remove(key)方法可以删除元素：
immutable.remove('Hello')
print('immutable =',immutable)  

#查询元素是否在集合中
print("'Hello' in student:",'Hello' in immutable)


print("\n\nAbout mutable and immutable")
a = ['c', 'b', 'a']
print ('a       :',a)  
a.sort()
print ('a.sort():',a)
print(" -------------- List is mutable")

a = 'abc'
print ('a                   :',a)  
print ("a.replace('a', 'A') :",a.replace('a', 'A')) 
print ('a                   :',a)
b = a.replace('a', 'A')
print ("b = a.replace('a', 'A')")  
print ('b                   :',b)   
print(" ---------So, string is mutable")

"""
要始终牢记的是，a是变量，而'abc'才是字符串对象！
有些时候，我们经常说，对象a的内容是'abc'，但其实是指，a本身是一个变量，它指向的对象的内容才是'abc'：
对于不变对象来说，调用对象自身的任意方法，也不会改变该对象自身的内容。
相反，这些方法会创建新的对象并返回，这样，就保证了不可变对象本身永远是不可变的。
"""














