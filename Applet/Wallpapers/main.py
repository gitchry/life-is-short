import os, shutil
from datetime import datetime

if os.path.exists('wallpapers') == False:
    os.mkdir('wallpapers')
# 把这个文件所在目录wallpapers文件夹作为保存图片的目录
save_folder = dir_path = os.path.dirname(
    os.path.realpath(__file__)) + '\wallpapers'
    
print(dir_path)
    
# 动态获取系统存放锁屏图片的位置
wallpaper_folder = os.getenv('LOCALAPPDATA') + (
    '\Packages\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy'
    '\LocalState\Assets')
# 列出所有的文件
wallpapers = os.listdir(wallpaper_folder)
for wallpaper in wallpapers:
    wallpaper_path = os.path.join(wallpaper_folder, wallpaper)
    # 小于150kb的不是锁屏图片
    if (os.path.getsize(wallpaper_path) / 1024) < 150:
        continue
    wallpaper_name = wallpaper + '.jpg'
    save_path = os.path.join(save_folder, wallpaper_name)
    shutil.copyfile(wallpaper_path, save_path)
    print('Save wallpaper ' + save_path)