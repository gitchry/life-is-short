#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# ---------------------------------------------------------------------------------------------------
# 子类 Subclass   基类 Base class
'''
在OOP程序设计中，当我们定义一个class的时候，可以从某个现有的class继承，
新的class称为子类（Subclass），而被继承的class称为基类、父类或超类（Base class、Super class）。

比如，我们已经编写了一个名为Animal的class，有一个run()方法可以直接打印：
'''
class Animal(object):
    def run(self):
        print('Animal is running...')
        
#当我们需要编写Dog和Cat类时，就可以直接从Animal类继承：        
class Dog(Animal):
    pass

class Cat(Animal):
    pass
'''
对于Dog来说，Animal就是它的父类，对于Animal来说，Dog就是它的子类。Cat和Dog类似。
继承有什么好处？最大的好处是子类获得了父类的全部功能。
由于Animial实现了run()方法，因此，Dog和Cat作为它的子类，什么事也没干，就自动拥有了run()方法：
'''
dog = Dog()
dog.run()

cat = Cat()
cat.run()
#当然，也可以对子类增加一些方法，比如Dog类：
class Dog(Animal):

    def eat(self):
        print('Eating meat...')


# ---------------------------------------------------------------------------------------------------
# 多态
'''
无论是Dog还是Cat，它们run()的时候，显示的都是Animal is running...，
符合逻辑的做法是分别显示Dog is running...和Cat is running...，
因此，对Dog和Cat类改进如下：
'''
class Dog(Animal):

    def run(self):
        print('Dog is running...')

class Cat(Animal):

    def run(self):
        print('Cat is running...')
'''
当子类和父类都存在相同的run()方法时，我们说，子类的run()覆盖了父类的run()，
在代码运行的时候，总是会调用子类的run()。这样，我们就获得了继承的另一个好处：多态。

要理解什么是多态，我们首先要对数据类型再作一点说明。
当我们定义一个class的时候，我们实际上就定义了一种数据类型。
我们定义的数据类型和Python自带的数据类型，比如str、list、dict没什么两样：
'''
a = list() # a是list类型
b = Animal() # b是Animal类型
c = Dog() # c是Dog类型

isinstance(b, Dog)      # => False
isinstance(c, Dog)      # => True
isinstance(c, Animal)   # => True
print()
'''
c不仅仅是Dog，c还是Animal！
因为Dog是从Animal继承下来的，当我们创建了一个Dog的实例c时，
我们认为c的数据类型是Dog没错，但c同时也是Animal也没错，Dog本来就是Animal的一种！

所以，在继承关系中，如果一个实例的数据类型是某个子类，那它的数据类型也可以被看做是父类。
要理解多态的好处，我们还需要再编写一个函数，这个函数接受一个Animal类型的变量：
'''
def run_twice(animal):
    animal.run()
    animal.run()

run_twice(Animal())
run_twice(Dog())
run_twice(Cat())
#看上去没啥意思，但是仔细想想，现在，如果我们再定义一个Tortoise类型，也从Animal派生：
class Tortoise(Animal):     #Tortoise 龟
    def run(self):
        print('Tortoise is running slowly...')
run_twice(Tortoise())
'''
你会发现，新增一个Animal的子类，不必对run_twice()做任何修改，实际上，任何依赖Animal作为参数的函数或者方法都可以不加修改地正常运行，原因就在于多态。

多态的好处就是，当我们需要传入Dog、Cat、Tortoise……时，我们只需要接收Animal类型就可以了，因为Dog、Cat、Tortoise……都是Animal类型，
然后，按照Animal类型进行操作即可。由于Animal类型有run()方法，因此，传入的任意类型，只要是Animal类或者子类，就会自动调用实际类型的run()方法，这就是多态的意思：

对于一个变量，我们只需要知道它是Animal类型，无需确切地知道它的子类型，就可以放心地调用run()方法，
而具体调用的run()方法是作用在Animal、Dog、Cat还是Tortoise对象上，由运行时该对象的确切类型决定，这就是多态真正的威力：

调用方只管调用，不管细节，而当我们新增一种Animal的子类时，只要确保run()方法编写正确，不用管原来的代码是如何调用的。


这就是著名的“开闭”原则：
对扩展开放：允许新增Animal子类；
对修改封闭：不需要修改依赖Animal类型的run_twice()等函数。

继承还可以一级一级地继承下来，就好比从爷爷到爸爸、再到儿子这样的关系。而任何类，最终都可以追溯到根类object，这些继承关系看上去就像一颗倒着的树。

'''


# ---------------------------------------------------------------------------------------------------
# 静态语言 vs 动态语言
'''
对于静态语言（例如Java/C++）来说，如果需要传入Animal类型，则传入的对象必须是Animal类型或者它的子类，否则，将无法调用run()方法。

对于Python这样的动态语言来说，则不一定需要传入Animal类型。我们只需要保证传入的对象有一个run()方法就可以了：
'''
class Timer(object):
    def run(self):
        print('Start...')
        
run_twice(Timer())
print()
'''
这就是动态语言的“鸭子类型”，它并不要求严格的继承体系，一个对象只要“看起来像鸭子，走起路来像鸭子”，那它就可以被看做是鸭子。

Python的“file-like object“就是一种鸭子类型。对真正的文件对象，它有一个read()方法，返回其内容。
但是，许多对象，只要有read()方法，都被视为“file-like object“。
许多函数接收的参数就是“file-like object“，你不一定要传入真正的文件对象，完全可以传入任何实现了read()方法的对象。

小结

继承可以把父类的所有功能都直接拿过来，这样就不必重零做起，子类只需要新增自己特有的方法，也可以把父类不适合的方法覆盖重写。

动态语言的鸭子类型特点决定了继承不像静态语言那样是必须的。
'''


# ---------------------------------------------------------------------------------------------------
# 多重继承
'''
继承是面向对象编程的一个重要的方式，因为通过继承，子类就可以扩展父类的功能。

Animal类层次的设计，假设我们要实现以下4种动物：
Dog - 狗；
Bat - 蝙蝠；
Parrot - 鹦鹉；
Ostrich - 鸵鸟。
如果按照哺乳动物和鸟类归类，我们可以设计出这样的类的层次：
动物
----哺乳
--------狗
--------蝙蝠
----卵生
--------鹦鹉
--------驼鸟
但是如果按照“能跑”和“能飞”来归类，我们就应该设计出这样的类的层次：
动物
----能跑
--------狗
--------驼鸟
----能飞
--------鹦鹉
--------蝙蝠
如果要把上面的两种分类都包含进来，我们就得设计更多的层次：
动物
----哺乳
--------能飞的哺乳
------------蝙蝠
--------能跑的哺乳
------------狗
----卵生
--------能飞的卵生
------------鹦鹉
--------能跑的卵生
------------驼鸟
如果要再增加“宠物类”和“非宠物类”，这么搞下去，类的数量会呈指数增长，很明显这样设计是不行的。

正确的做法是采用多重继承。首先，主要的类层次仍按照哺乳类和鸟类设计：
'''
class Animal(object):
    pass

# 大类:
class Mammal(Animal):   #哺乳类
    pass

class Bird(Animal):     #卵生类
    pass

#现在，我们要给动物再加上Runnable和Flyable的功能，只需要先定义好Runnable和Flyable的类：
class Runnable(object):
    def run(self):
        print('Running...')

class Flyable(object):
    def fly(self):
        print('Flying...')
#对于需要Runnable功能的动物，就多继承一个Runnable，例如Dog：
class Dog(Mammal, Runnable):
    pass

#对于需要Flyable功能的动物，就多继承一个Flyable，例如Bat：
class Bat(Mammal, Flyable):
    pass
#通过多重继承，一个子类就可以同时获得多个父类的所有功能。

# ---------------------------------------------------------------------------------------------------
# MixIn
'''
在设计类的继承关系时，通常，主线都是单一继承下来的，
例如，Ostrich继承自Bird。但是，
如果需要“混入”额外的功能，通过多重继承就可以实现，
比如，让Ostrich除了继承自Bird外，再同时继承Runnable。

这种设计通常称之为MixIn。

为了更好地看出继承关系，我们把Runnable和Flyable改为RunnableMixIn和FlyableMixIn。
类似的，你还可以定义出肉食动物CarnivorousMixIn和植食动物HerbivoresMixIn，让某个动物同时拥有好几个MixIn：
'''
class Dog(Mammal, RunnableMixIn, CarnivorousMixIn):
    pass
    '''
MixIn的目的就是给一个类增加多个功能，这样，在设计类的时候，
我们优先考虑通过多重继承来组合多个MixIn的功能，而不是设计多层次的复杂的继承关系。

Python自带的很多库也使用了MixIn。举个例子，Python自带了TCPServer和UDPServer这两类网络服务，
而要同时服务多个用户就必须使用多进程或多线程模型，这两种模型由ForkingMixIn和ThreadingMixIn提供。
通过组合，我们就可以创造出合适的服务来。

比如，编写一个多进程模式的TCP服务，定义如下：    
'''
class MyTCPServer(TCPServer, ForkingMixIn):
    pass

#编写一个多线程模式的UDP服务，定义如下：

class MyUDPServer(UDPServer, ThreadingMixIn):
    pass

#如果你打算搞一个更先进的协程模型，可以编写一个CoroutineMixIn：

class MyTCPServer(TCPServer, CoroutineMixIn):
    pass

'''
这样一来，我们不需要复杂而庞大的继承链，只要选择组合不同的类的功能，就可以快速构造出所需的子类。

小结

由于Python允许使用多重继承，因此，MixIn就是一种常见的设计。

只允许单一继承的语言（如Java）不能使用MixIn的设计。
'''



