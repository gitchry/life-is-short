#!/usr/bin/env python3
# -*- coding: utf-8 -*-

' a Hello class module '

__author__ = 'chry'

class Hello(object):
    def hello(self, name='world'):
        print('Hello, %s.' % name)
        
if __name__=='__main__':
    sss = Hello()
    sss.hello()