#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#pass是empty语句,什么都不干,相当于C语言的分号
pass


    
#while  

while True:
    age = input('please input your English grade:')
    if age.isdigit() and int(age) <= 100 and int(age) >= 0:
        if int(age) == 0:
            print('Are you killed me ?')
            continue
        if int(age) < 60:
            print('so bad...')
            break;
        elif int(age) < 90:
            print('nice...')
            break;
        else:
            print('holy shit!')
            break;
    elif age == '':
        print('quit')
        break
    else:
        print('Please enter the correct number(0-100)')
else:
    pass
print()  
 
'''
for <variable> in <sequence>:
    <statements>
else:
    <statements>

for x in list: 就是把list的每个元素代入变量x，然后执行缩进块(或者冒号后面)的语句。
'''  

classmates = ('Michael', 'Bob', 'Tracy')
for name in classmates:
    print(name)

sum = 0
for x in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]:
    sum = sum + x
print(sum)

#range()函数,生成数列  可用于列表创建生成L = list(range(5))
print(list(range(10)))
print(list(range(1,11)))
print(list(range(1,11,2)))
print(list(range(10,-20,-3)))


print ('\n')      
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, '=', x, '*', n//x)
            break
    else:
        # 循环中没有找到元素
        print(n, '是质数')      
print() 



