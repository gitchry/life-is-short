#!/usr/bin/env python3
# -*- coding: utf-8 -*-


#函数作为返回值
#高阶函数除了可以接受函数作为参数外，还可以把函数作为结果值返回。

def lazy_sum(*args):
    def sum():
        ax = 0
        for n in args:
            ax = ax + n
        return ax
    return sum

f1= lazy_sum(1, 3, 5, 7, 9)
print(f1)
print(f1())
#当我们调用lazy_sum()时，每次调用都会返回一个新的函数，即使传入相同的参数：

f2 = lazy_sum(1, 3, 5, 7, 9)
print(f1 == f2)

"""
我们在函数lazy_sum中又定义了函数sum，并且，内部函数sum可以引用外部函数lazy_sum的参数和局部变量，
当lazy_sum返回函数sum时，相关参数和变量都保存在返回的函数中，
这种称为“闭包（Closure）”的程序结构拥有极大的威力。
"""

# ---------------------------------------------------------------------------------------------------
# 闭包
"""
注意到返回的函数在其定义内部引用了局部变量args，
所以，当一个函数返回了一个函数后，其内部的局部变量还被新函数引用，
所以，闭包用起来简单，实现起来可不容易。
另一个需要注意的问题是，返回的函数并没有立刻执行，而是直到调用了f()才执行。
"""
def count():
    fs = []
    for i in range(1, 4):
        def f():
             return i*i
        fs.append(f)
    return fs

f1, f2, f3 = count()
#你可能认为调用f1()，f2()和f3()结果应该是1，4，9，但实际结果是：
print(f1(),end = ' ')
print(f2(),end = ' ')
print(f3(),end = ' ')
print()
"""
原因就在于返回的函数引用了变量i，但它并非立刻执行。
等到3个函数都返回时，它们所引用的变量i已经变成了3，因此最终结果为9。
返回闭包时牢记的一点就是：返回函数不要引用任何循环变量，或者后续会发生变化的变量。
如果一定要引用循环变量怎么办？
方法是再创建一个函数，用该函数的参数绑定循环变量当前的值，无论该循环变量后续如何更改，已绑定到函数参数的值不变：
"""
def count():
    def f(j):
        def g():
            return j*j
        return g
    fs = []
    for i in range(1, 4):
        fs.append(f(i)) # f(i)立刻被执行，因此i的当前值被传入f()
    return fs
    
f1, f2, f3 = count()
print(f1(),end = ' ')
print(f2(),end = ' ')
print(f3(),end = ' ')
print()

#缺点是代码较长，可利用lambda函数缩短代码。

"""
小结

一个函数可以返回一个计算结果，也可以返回一个函数。

返回一个函数时，牢记该函数并未执行，返回函数中不要引用任何可能会变化的变量。
"""
# ---------------------------------------------------------------------------------------------------
# 匿名函数 lambda
print(list(map(lambda x: x * x, [1, 2, 3, 4, 5, 6, 7, 8, 9])))
"""
关键字lambda表示匿名函数，冒号前面的x表示函数参数。
匿名函数有个限制，就是只能有一个表达式，不用写return，返回值就是该表达式的结果。
用匿名函数有个好处，因为函数没有名字，不必担心函数名冲突。此外，匿名函数也是一个函数对象，也可以把匿名函数赋值给一个变量，再利用变量来调用该函数：
"""
f = lambda x: x * x
print(f)
print(f(4))

#同样，也可以把匿名函数作为返回值返回，比如：
def build(x, y):
    return lambda: x * x + y * y

print(build(3,5))
print(build(3,5)(),'\n')

# ---------------------------------------------------------------------------------------------------
# 装饰器 Decorator
def now():
    print('2017年1月3日 23:15:16')
#函数对象有一个__name__属性，可以拿到函数的名字：
print("now.__name__:",now.__name__)

"""
现在，假设我们要增强now()函数的功能，比如，在函数调用前后自动打印日志，
但又不希望修改now()函数的定义，这种在代码运行期间动态增加功能的方式，称之为“装饰器”（Decorator）。

本质上，decorator就是一个返回函数的高阶函数。所以，我们要定义一个能打印日志的decorator，可以定义如下：
"""
def log(func):                  #此时func就是now
    def wrapper(*args, **kw):   #wrapper的入参是什么?
        print('call %s():' % func.__name__)
        return func(*args, **kw)
    return wrapper

#观察上面的log，因为它是一个decorator，所以接受一个函数作为参数，并返回一个函数。我们要借助Python的@语法，把decorator置于函数的定义处：
@log
def now():
    print('2017年1月3日 23:15:16\n')

print("now.__name__:",now.__name__)    
now()
"""
#把@log放到now()函数的定义处，相当于执行了语句：now = log(now)
def now():
    print('2017年1月3日 23:15:16')
    
now = log(now)
now()

由于log()是一个decorator，返回一个函数，所以，原来的now()函数仍然存在，
只是现在同名的now变量指向了新的函数，于是调用now()将执行新函数，即在log()函数中返回的wrapper()函数。

wrapper()函数的参数定义是(*args, **kw)，因此，wrapper()函数可以接受任意参数的调用。
在wrapper()函数内，首先打印日志，再紧接着调用原始函数。

如果decorator本身需要传入参数，那就需要编写一个返回decorator的高阶函数，写出来会更复杂。比如，要自定义log的文本：
"""

def log(text):
    def decorator(func):
        def wrapper(*args, **kw):
            print('%s %s():' % (text, func.__name__))
            return func(*args, **kw)
        return wrapper
    return decorator
#这个3层嵌套的decorator用法如下：
@log('execute')
def now():
    print('2017年1月3日 23:15:16\n')
   
now()
"""
和两层嵌套的decorator相比，3层嵌套的效果是这样的： now = log('execute')(now)

我们来剖析上面的语句，首先执行log('execute')，返回的是decorator函数，再调用返回的函数，参数是now函数，返回值最终是wrapper函数。

以上两种decorator的定义都没有问题，但还差最后一步。因为我们讲了函数也是对象，它有__name__等属性，但你去看经过decorator装饰之后的函数，它们的__name__已经从原来的'now'变成了'wrapper'：

>>> now.__name__
'wrapper'

因为返回的那个wrapper()函数名字就是'wrapper'，所以，需要把原始函数的__name__等属性复制到wrapper()函数中，否则，有些依赖函数签名的代码执行就会出错。

不需要编写wrapper.__name__ = func.__name__这样的代码，Python内置的functools.wraps就是干这个事的，所以，一个完整的decorator的写法如下：

"""

import functools

def log(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        print('call %s():' % func.__name__)
        return func(*args, **kw)
    return wrapper
    
@log
def now():
    print('2017年1月3日 23:15:16\n')
    
print("now.__name__:",now.__name__) 
now()
#或者针对带参数的decorator：

import functools

def log(text):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kw):
            print('%s %s():' % (text, func.__name__))
            return func(*args, **kw)
        return wrapper
    return decorator
@log('full execute log')
def now():
    print('2017年1月3日 23:15:16\n')
    
now()
"""
import functools是导入functools模块。模块的概念稍候讲解。现在，只需记住在定义wrapper()的前面加上@functools.wraps(func)即可。

小结

在面向对象（OOP）的设计模式中，decorator被称为装饰模式。OOP的装饰模式需要通过继承和组合来实现，
而Python除了能支持OOP的decorator外，直接从语法层次支持decorator。Python的decorator可以用函数实现，也可以用类实现。

decorator可以增强函数的功能，定义起来虽然有点复杂，但使用起来非常灵活和方便。

请编写一个decorator，能在函数调用的前后打印出'begin call'和'end call'的日志。
"""

import functools

def log(func):
    @functools.wraps(func)
    def wraps(*args, **kw):
        print('begin call')
        func(*args, **kw)
        print('end call')
    return wraps
@log
def now(txt):
    print(txt)
    
now('hello\n\n')


#再思考一下能否写出一个@log的decorator，使它既支持：@log  又支持: @log('execute')

def log(f):
    if isinstance(f, str):
        def decorator(func):
            @functools.wraps(func)
            def wrapper(*args, **kw):
                print(f,'begin call')
                func(*args, **kw)
                print('end call')
            return wrapper
        return decorator
    else:
        @functools.wraps(f)
        def wraps(*args, **kw):
            print('begin call')
            f(*args, **kw)
            print('end call')
        return wraps
    
@log('full execute log')
def now():
    print("@log('full execute log')")
    
now()
    

@log
def now():
    print("@log")
    
now()


# ---------------------------------------------------------------------------------------------------
# 偏函数 Partial function
"""
Python的functools模块提供了很多有用的功能，其中一个就是偏函数（Partial function）。(注意:和数学意义上的偏函数不一样。)

"""
print(int('56'))            #默认按十进制转换
print(int('56',base = 8))   #按八进制转换
print(int('56',base = 16))  #按十六进制转换

#假设要转换大量的二进制字符串，每次都传入int(x, base=2)非常麻烦，于是，我们想到，可以定义一个int2()的函数，默认把base=2传进去：
def int2(x, base=2):
    return int(x, base)

print('1000000 =>',int2('1000000'))
#functools.partial就是帮助我们创建一个偏函数的，不需要我们自己定义int2()，可以直接使用下面的代码创建一个新的函数int2：

import functools
int2 = functools.partial(int, base=2)
print('1000000 =>',int2('1000000'))

"""
所以，简单总结functools.partial的作用就是，把一个函数的某些参数给固定住（也就是设置默认值），返回一个新的函数，调用这个新函数会更简单。

注意到上面的新的int2函数，仅仅是把base参数重新设定默认值为2，但也可以在函数调用时传入其他值：
"""

print('1000000 =>',int2('1000000',base=8))

"""
最后，创建偏函数时，实际上可以接收函数对象、*args和**kw这3个参数，当传入：  
int2 = functools.partial(int, base=2)

实际上固定了int()函数的关键字参数base，也就是：  
int2('10010')

相当于：
"""

kw = { 'base': 2 }
print(int('10010', **kw))



max2 = functools.partial(max, 10)
print(max2(5, 6, 7))

#相当于：

args = (10, 5, 6, 7)
print(max(*args))
"""
结果为10。


小结

当函数的参数个数太多，需要简化时，使用functools.partial可以创建一个新的函数，这个新函数可以固定住原函数的部分参数，从而在调用时更简单。

当你去采购，要买 显示器、硬盘、主板、显卡、内存、电源、机箱、相机。。 这么多怎么记得住，于是你转化了下，你要买 电脑+相机。

"""





















